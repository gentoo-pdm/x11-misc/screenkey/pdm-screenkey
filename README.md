Single package Gentoo ebuild repository for screenkey; A screencast tool to display your keys inspired by Screenflick.

HOMEPAGE="https://www.thregr.org/~wavexx/software/screenkey/"

REPOSITORY="https://gitlab.com/screenkey/screenkey"



Layman package installation required.

File "pdm-screenkey.xml" from repository root directory must be integrated as documented in https://wiki.gentoo.org/wiki/Layman, section Adding custom repositories.

Possible corresponding command line: 'layman -o https://gitlab.com/gentoo-pdm/x11-misc/screenkey/pdm-screenkey.xml -f -a pdm-screenkey'.

Repository activation: 'layman -a pdm-screenkey'.

Package trial install: 'emerge -1 screenkey', to be removed at next run of 'emerge -c'.

Package permanent install: 'emerge screenkey' if not installed, 'emerge screenkey --noreplace' post trial former step.