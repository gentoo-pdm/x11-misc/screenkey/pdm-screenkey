# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Based on x11-misc/screenkey from src_prepare-overlay overlay

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11} )

inherit distutils-r1 xdg-utils

DESCRIPTION="A screencast tool to display your keys inspired by Screenflick"
HOMEPAGE="https://www.thregr.org/~wavexx/software/screenkey/"

if [[ "${PV}" == *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.com/screenkey/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://www.thregr.org/~wavexx/software/${PN}/releases/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

RESTRICT="mirror"
LICENSE="GPL-3+"
SLOT="0"
IUSE="appindicator multimedia"

#	dev-python/python-distutils-extra[${PYTHON_USEDEP}]
#	dev-python/setuptools[${PYTHON_USEDEP}]
#	sys-devel/gettext
BDEPEND="
	dev-python/Babel[${PYTHON_USEDEP}]
	dev-python/wheel[${PYTHON_USEDEP}]
"

RDEPEND="
	dev-python/dbus-python[${PYTHON_USEDEP}]
	dev-python/pycairo[${PYTHON_USEDEP}]
	dev-python/pygobject[${PYTHON_USEDEP}]
	x11-libs/gtk+:3[X,introspection]
	x11-misc/slop
	appindicator? ( dev-libs/libappindicator:3 )
	multimedia? ( media-fonts/fontawesome )
"

src_prepare() {
	default
	sed -i.bck "s#share/doc/screenkey#share/doc/${PF}#" setup.py || die
	xdg_environment_reset
}

# TODO: localizing
# src_configure() {
#		econf \
#				$(use_enable nls)
#				--with-x
#}

python_install() {
	distutils-r1_python_install
}

pkg_postinst() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
}
